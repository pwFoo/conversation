## Conversation module template code
template fields: title, body
~~~~
<?php

$talk = $modules->get('Conversation');
$baseUrl = "{$config->urls->root}conversation";

echo "<h1>Conversation module</h1><a href='{$baseUrl}'>Show conversation list</a> || <a href='{$baseUrl}/add'>Add conversation</a><hr />";

if ($input->urlSegment1 == 'add') {
    echo $talk->add($pages->get('/conversation/'));
}
elseif($input->urlSegment1 == 'edit') {
    echo $talk->edit($page);
}
elseif ($page->id && $page->name != 'conversation') {
    $topic = $page;
    echo "<h2>{$topic->title}</h2><div>{$topic->body}<p><a href='{$topic->url}edit'>Edit conversation</a></p></div>";
}
elseif($input->urlSegment1) {
    $tid = explode('-', $input->urlSegment1);
    $topic = $pages->get($tid[0]);
    
    echo "<h2>{$topic->title}</h2><div>{$topic->body}<p><a href='{$topic->url}edit'>Edit conversation</a></p></div>";
}
else {
    echo "<h2>Conversation list</h2>";
    
    foreach ($pages->get('/conversation/')->children as $topic) {
        echo "<b><a href='{$topic->name}'>{$topic->title}</a></b> <i>(by {$topic->createdUser->name})</i><br />";
    }
}
~~~~